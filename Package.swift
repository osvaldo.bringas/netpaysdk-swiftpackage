// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "NetPaySDKPackage",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "NetPaySDK",
            targets: ["NetPaySDK"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
            .binaryTarget(
                name: "NetPaySDK",
                url: "https://gitlab.com/osvaldo.bringas/netpaysdk-distribution/-/blob/master/NetPaySDKPackage.zip", checksum: "0e3cf150bf1d724d5606b621a211cbf08b32b418118e2d19ad9829c894952734")
        ]
)
